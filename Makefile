## TeX engine
# Use lualatex
TEX = lualatex --shell-escape
# Use luajittex (Please see https://texwiki.texjp.org/?LuajitTeX)
#TEX = luajittex --fmt=luajitlatex.fmt
TEXOPTION =

## BiBTeX
BIBTEX = pbibtex

## Remove command
RM= rm -f

##  Filename
TITLE = openfoam-code-verification-errata

SRC = $(TITLE).tex
BBL = $(TITLE).bbl
PDF = $(TITLE).pdf
AUX = $(TITLE).aux
LOG = $(TITLE).log
BLG = $(TITLE).blg
OUT = $(TITLE).out
BIB = $(TITLE).bib

CHANGED = grep '^LaTeX Warning: Label(s) may have changed.'

all: $(PDF)

$(PDF): $(SRC)
	$(TEX) $(TEXOPTION) $(SRC)
	(while $(CHANGED) $(LOG); do $(TEX) $(TEXOPTION) $(SRC); done)

clean:
	$(RM) $(LOG) $(AUX) $(LOG) $(BLG) $(OUT) $(PDF) $(DOC)

distclean:
	$(RM) $(LOG) $(AUX) $(LOG) $(BLG) $(OUT)
